package org.iu.dspwa042101.uebungsprojekt.simplewebconnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;


/**
 * Hinweise zu untenstehendem Code:
 * --------------------------------
 * Untenstehendes Beispiel ist Teil eins sehr einfachen Projektes, welches einen Anfrage-/Antwort-Server realisiert.
 * Um den Blick auf die Standard-Funktionen der Socket-Programmierung zu lenken, wurden häufig verwendete
 * Mechanismen, z. B. zum Umgang mit BufferedReader etc. außer Acht gelassen. Für professionelle Programmierung
 * sollten derartige Mechanismen natürlich berücksichtigt werden. Bitte beachten Sie insofern, dass es sich hierbei um ein
 * unvollständiges Lehrbeispiel handelt. Bitte lesen Sie die Ausführungen bei [Cz21] bzw. sonstiger Literatur nach.
 * <p>
 * Eine gute Erklärung für die Programmierung mit Sockets findet sich bei [Cz21].
 * In untenstehendem Code werden dieselben Java-Standardbefehle verwendet, die bei [Cz21] erklärt sind.
 * <p>
 * [Cz21] Czeschla, Jörg (o. J.): "Wie schreibe ich einen Server und einen Client, die über Sockets miteinander kommunizieren?".
 * aus den Internetinformationen von javabeginners unter der URL: https://javabeginners.de/Netzwerk/Socketverbindung.php
 * [Stand: 20.12.2021, copyright javabeginners 2021].
 */
public class SimpleWebServer {

    public static void main(String[] args) {

        try {
            int port = 80;
            ServerSocket serverSocket = new ServerSocket(port);

            while (true) {
                // Create a new socket to handle the request
                Socket socket = serverSocket.accept();

                // Debug the request from the client
                System.out.println("+++ Received request +++");
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String line;
                // Skip the request body (if any) by reading only until the first blank line
                while (!Objects.equals(line = br.readLine(), "")) {
                    System.out.println(line);
                }

                // Get the current system time in the format HH:mm:ss
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String response = sdf.format(new Date());

                // Answer the request with the current system time
                PrintStream ps = new PrintStream(socket.getOutputStream(), true);
                ps.println("HTTP/1.1 200 OK");
                ps.println("Content-Type: text/plain");
                ps.println("Content-Length: " + response.length());
                ps.println();
                ps.println(response);
                System.out.println("+++ Answered request with system time +++");
                System.out.println("----------------------------------");

                // Close the socket connection
                socket.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
